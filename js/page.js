$(function(){
	init();
	$('body').jpreLoader({
		splashID: "#jSplash",
		loaderVPos: '50%',
		splashVPos: '45%',
		autoClose: true
	}, function() {
		OPEN();
	});
	MOVIE();
	MN();
	// BTN();
});


// function BTN(){
// 	$( ".p01_06 " ).hover(
// 	  function() {
// 	    TweenMax.to($(this),0.3,{scaleX:0.9,scaleY:0.9});
// 	  }, function() {
// 	    TweenMax.to($(this),0.3,{scaleX:1,scaleY:1});
// 	  }
// 	);
// }

function init(){
	TweenMax.set($('.p01_01'),{scaleX:0.5,scaleY:0.5,opacity:0});
	TweenMax.set($('.p01_06'),{scaleX:1.3,scaleY:1.3,opacity:0});
	TweenMax.set($('.p01_02'),{rotation:-60,opacity:0});
	TweenMax.set($('.p01_05'),{rotation:60,opacity:0});
	TweenMax.set($('.p01_03'),{left:0,opacity:0});
	TweenMax.set($('.p01_04'),{right:0,opacity:0});
}



function OPEN(){
	// TweenMax.fromTo($('.p01_02'),0.5,{rotation:-90,opacity:0},{rotation:0,opacity:1,transformOrigin:'bottom center'});
	// TweenMax.fromTo($('.p01_05'),0.5,{rotation:90,opacity:0},{rotation:0,opacity:1,transformOrigin:'bottom center'});
	TweenMax.to($('.p01_02,.p01_05'),0.3,{rotation:0,opacity:1,transformOrigin:'bottom center',delay:0.2});
	TweenMax.to($('.p01_04'),0.5,{right:260,opacity:1,delay:1,ease:Quint.easeOut});
	TweenMax.to($('.p01_03'),0.5,{left:297,opacity:1,delay:1,ease:Quint.easeOut});
	TweenMax.to($('.p01_01'),0.3,{scaleX:1,scaleY:1,opacity:1,delay:1});
	TweenMax.to($('.p01_06'),0.3,{scaleX:1,scaleY:1,opacity:1,delay:1.2});
}


function MOVIE(){
 	$('.p01_02').on("click", function() {
 	  	$(".popup").fadeIn();
 	});

 	$('.videobtn').on("click", function() {
 	  	$(this).css({'display':'none'});
 	  	$('.video').append('<iframe width="536" height="358" src="https://www.youtube.com/embed/h5HVhVvTBCk?rel=0&autoplay=1" frameborder="0" allowfullscreen></iframe>');
 	});

 	$('.close').on("click", function() {
 	  	$(".popup").fadeOut();
 	  	$(".videobtn").css({'display':'block'});
 	  	$('.video').empty();
 	});
 }

function MN(){
	$('.menu ul li:nth-of-type(2) a').on("click", function() {
 	  	RECLASS();
 	  	init();
 	  	$(this).addClass("add");	
 	  	$(".part03").fadeIn();
 	});
 	$('.menu ul li:nth-of-type(4) a , .menu ul li:nth-of-type(5) a ').on("click", function() {
 		$(".menu ul li:nth-of-type(1) a ,.menu ul li:nth-of-type(2) a ,.menu ul li:nth-of-type(3) a ,.menu ul li:nth-of-type(4) a ,.menu ul li:nth-of-type(5) a ").removeClass("add");
 	  	$(this).addClass("add");
 	});
 	$('.menu ul li:nth-of-type(3) a').on("click", function() {
 	  	RECLASS();
 	  	init();
 	  	$(this).addClass("add");
 	  	$(".part02").fadeIn();
 	});
 	$('.menu ul li:nth-of-type(1) a').on("click", function() {
 	  	RECLASS();
 	  	init();
 	  	$(this).addClass("add");
 	  	$(".part01").fadeIn(0,function() {
 	  		OPEN();
 	  	});	  	
 	});

}

function RECLASS(){
 	$(".menu ul li:nth-of-type(1) a ,.menu ul li:nth-of-type(2) a ,.menu ul li:nth-of-type(3) a ,.menu ul li:nth-of-type(4) a , .menu ul li:nth-of-type(5) a ").removeClass("add");
 	$(".part01 , .part02 , .part03").fadeOut(0);
}
